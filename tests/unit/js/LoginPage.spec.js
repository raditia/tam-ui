import Vuetify from 'vuetify'
import Vuex from 'vuex'
import LoginPage from '../../../src/components/user/LoginPage'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
// import Swal from 'sweetalert2'

describe('LoginPage.vue', () => {
  const localVue = createLocalVue()
  const router = new VueRouter()
  const actions = {
    postLoginRequest: jest.fn()
  }

  localVue.use(router)
  localVue.use(Vuetify)
  localVue.use(Vuex)

  const store = new Vuex.Store({
    state: {},
    actions
  })

  const wrapper = shallowMount(LoginPage, {
    localVue: localVue,
    router,
    store
  })

  it('should successfully render login page', function () {
    expect(wrapper.name()).toEqual('LoginPage')
  })

  it('should be able to login as user', function () {
    const loginDataUser = {
      'requestId': 0,
      'errorMessage': '',
      'errorCode': '',
      'success': true,
      'value': {
        'token': '95d21103-c9ac-43f5-bab8-925b57fb487e',
        'member': {
          'id': 'c0c16713-5bb0-4d6b-b6c9-6848acd54f10',
          'name': 'Member 10',
          'img': '10',
          'email': 'member10@tam.com',
          'password': null,
          'available': true,
          'jobLevel': {
            'id': 3,
            'name': 'Senior SDE'
          },
          'label': {
            'id': 3,
            'name': 'Fullstack'
          },
          'role': {
            'id': 1,
            'name': 'Manager'
          }
        }
      }
    }
    window.location.assign = jest.fn() // Create a spy
    const wrapper = shallowMount(LoginPage, {
      localVue: localVue,
      router,
      store,
      computed: {
        loginData: jest.fn(() => {
          return loginDataUser
        })
      }
    })

    let params = {
      email: 'member10@tam.com',
      password: 'member10'
    }

    wrapper.setData({ params: params })
    wrapper.vm.postLoginRequest()
    wrapper.vm.setLoginData()
    expect(wrapper.vm.loginData.value.member.role.name === 'Manager')
    expect(window.location.assign).toHaveBeenCalledWith('/dashboard')
  })

  it('should be able to login as member', function () {
    const loginDataMember = {
      'requestId': 0,
      'errorMessage': '',
      'errorCode': '',
      'success': true,
      'value': {
        'token': 'f2add79e-591f-4052-8418-f10809b0b93b',
        'member': {
          'id': 'c0c16713-5bb0-4d6b-b6c9-6848acd54ff1',
          'name': 'Member 01',
          'img': '01',
          'email': 'member01@tam.com',
          'password': null,
          'available': true,
          'jobLevel': {
            'id': 1,
            'name': 'Associate SDE'
          },
          'label': {
            'id': 1,
            'name': 'Front-end'
          },
          'role': {
            'id': 2,
            'name': 'Member'
          }
        }
      }
    }
    window.location.assign = jest.fn() // Create a spy
    const wrapper = shallowMount(LoginPage, {
      localVue: localVue,
      router,
      store,
      computed: {
        loginData: jest.fn(() => {
          return loginDataMember
        })
      }
    })

    let params = {
      email: 'member01@tam.com',
      password: 'member01'
    }

    wrapper.setData({ params: params })
    wrapper.vm.postLoginRequest()
    wrapper.vm.setLoginData()
    expect(wrapper.vm.loginData.value.member.role.name === 'Member')
    expect(window.location.assign).toHaveBeenCalledWith('/member')
  })
})
