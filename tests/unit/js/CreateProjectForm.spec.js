import Vuetify from 'vuetify'
import Vuex from 'vuex'
import CreateProjectForm from '../../../src/components/form/UserCreateProject'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'

describe('CreateProjectForm.vue', () => {
  const localVue = createLocalVue()
  const router = new VueRouter()
  const actions = {
    postProjectInit: jest.fn(),
    getProjectDomain: jest.fn(),
    getMemberLabel: jest.fn()
  }

  localVue.use(router)
  localVue.use(Vuetify)
  localVue.use(Vuex)

  const store = new Vuex.Store({
    state: {},
    actions
  })

  const wrapper = shallowMount(CreateProjectForm, {
    localVue: localVue,
    router,
    store
  })

  it('should successfully render the page', function () {
    expect(wrapper.name()).toEqual('create-project-form')
  })

  it('should able to create project', function () {
    let memberLabelData = {
      'requestId': 10001,
      'errorMessage': '',
      'errorCode': '',
      'success': true,
      'value': [
        {
          'id': 1,
          'name': 'Front-end'
        },
        {
          'id': 2,
          'name': 'Back-end'
        },
        {
          'id': 3,
          'name': 'Fullstack'
        },
        {
          'id': 4,
          'name': 'Quality-Assurance'
        },
        {
          'id': 5,
          'name': 'Android'
        },
        {
          'id': 6,
          'name': 'iOS'
        },
        {
          'id': 7,
          'name': 'Tech-Lead'
        }
      ]
    }

    let projectDomainData = {
      'requestId': 10001,
      'errorMessage': '',
      'errorCode': '',
      'success': true,
      'value': [
        {
          'id': 'c0c16713-5bb0-4d6b-b6c9-6848acd54ff1',
          'name': 'Payment'
        },
        {
          'id': 'c0c16713-5bb0-4d6b-b6c9-6848acd54ff2',
          'name': 'Fulfillment'
        },
        {
          'id': 'c0c16713-5bb0-4d6b-b6c9-6848acd54ff3',
          'name': 'Logistic'
        },
        {
          'id': 'c0c16713-5bb0-4d6b-b6c9-6848acd54ff4',
          'name': 'Merchant'
        },
        {
          'id': 'c0c16713-5bb0-4d6b-b6c9-6848acd54ff5',
          'name': 'Commerce'
        },
        {
          'id': 'c0c16713-5bb0-4d6b-b6c9-6848acd54ff6',
          'name': 'Analytics'
        },
        {
          'id': 'c0c16713-5bb0-4d6b-b6c9-6848acd54ff7',
          'name': 'Finance'
        }
      ]
    }

    const wrapper = shallowMount(CreateProjectForm, {
      localVue: localVue,
      router,
      store,
      computed: {
        projectDomainList: jest.fn(() => {
          return projectDomainData
        }),
        memberLabel: jest.fn(() => {
          return memberLabelData
        })
      }
    })
    let params = {
      'projectName': 'X-order',
      'description': 'this project is meant to create order API service',
      'startDate': '2018-06-05',
      'endDate': '2018-08-05',
      'durationDetermined': false,
      'labelNeeded': [
        {
          'id': 1,
          'memberLabel': 'Front-end',
          'count': 1
        }
      ],
      'projectDomain': [
        {
          'id': 'c0c16713-5bb0-4d6b-b6c9-6848acd54ff1',
          'name': 'Payment'
        },
        {
          'id': 'c0c16713-5bb0-4d6b-b6c9-6848acd54ff2',
          'name': 'Warehouse'
        }
      ]
    }

    wrapper.setData({ params: params })
    wrapper.vm.goToRecommendation()
    wrapper.vm.postProjectInit()
  })
})
