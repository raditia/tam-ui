import Vuetify from 'vuetify'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import UserDashboard from '../../../src/components/user/UserDashboard'

describe('UserDashboard.vue', () => {
  const localVue = createLocalVue()
  const router = new VueRouter()

  localVue.use(router)
  localVue.use(Vuetify)

  const wrapper = shallowMount(UserDashboard, {
    localVue: localVue,
    router
  })

  it('should successfully render user dashboard page', function () {
    const spy = jest.spyOn(wrapper.vm, 'goToDashboard')

    expect(wrapper.name()).toEqual('UserDashboard')
    expect(spy).toBeCalled()
  })
})
