import Vuetify from 'vuetify'
import UserCreateProject from '../../../src/components/form/UserCreateProject'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'

describe('UserCreateProject.vue', () => {
  const localVue = createLocalVue()
  const router = new VueRouter()

  localVue.use(router)
  localVue.use(Vuetify)

  it('should successfully render create project form page', function () {
    const wrapper = shallowMount(UserCreateProject, {
      localVue: localVue,
      router
    })
    expect(wrapper.name()).toEqual('create-project-form')
  })

  it('should be able to post new project', function () {
    const params = {
      projectName: 'X-order',
      description: 'this project is meant to create order API service',
      startDate: '2018-06-05',
      endDate: '2018-08-05',
      durationDetermined: false,
      labelNeeded: [
        {
          id: 1,
          memberLabel: 'Front-end',
          count: 1
        }
      ],
      projectDomain: [
        {
          id: 'c0c16713-5bb0-4d6b-b6c9-6848acd54ff1',
          name: 'Payment'
        },
        {
          id: 'c0c16713-5bb0-4d6b-b6c9-6848acd54ff2',
          name: 'Warehouse'
        }
      ]
    }
    const recommendationData = {
      'requestId': 1010,
      'errorMessage': '',
      'errorCode': '',
      'success': true,
      'value': {
        'projectName': 'X-order',
        'description': 'this project is meant to create order API service',
        'startDate': '2018-06-05T00:00:00.000+0000',
        'endDate': '2018-08-05T00:00:00.000+0000',
        'durationDetermined': false,
        'labelNeeded': [
          {
            'id': 1,
            'memberLabel': null,
            'count': 1
          }
        ],
        'projectDomain': [
          {
            'id': 'c0c16713-5bb0-4d6b-b6c9-6848acd54ff1',
            'name': 'Payment'
          },
          {
            'id': 'c0c16713-5bb0-4d6b-b6c9-6848acd54ff2',
            'name': 'Warehouse'
          }
        ],
        'members': [
          {
            'id': 'c0c16713-5bb0-4d6b-b6c9-6848acd54ff2',
            'img': '02',
            'name': 'Member 02',
            'email': 'member02@tam.com',
            'jobLevel': 'SDE',
            'label': 'Front-end',
            'status': true,
            'records': {
              'rateScore': 3.5,
              'responsibilityScore': 4.0,
              'communicationScore': 3.5,
              'teamWorkScore': 4.0,
              'problemSolvingScore': 3.5,
              'leadershipScore': 4.5,
              'domainScore': 3.5,
              'technicalScore': 3.5
            },
            'projectHistory': [
              {
                'projectId': 'c0c16713-5bb0-4d6b-b6c9-6848acd54ff2',
                'projectName': 'Project 02',
                'description': 'Project unit 02',
                'projectDomain': [
                  {
                    'id': 'c0c16713-5bb0-4d6b-b6c9-6848acd54ff2',
                    'name': 'Warehouse'
                  },
                  {
                    'id': 'c0c16713-5bb0-4d6b-b6c9-6848acd54ff3',
                    'name': 'Logistic'
                  }
                ],
                'startDate': '2018-10-31T17:00:00.000+0000',
                'endDate': '2018-12-01T17:00:00.000+0000',
                'numberOfIndividuals': 4,
                'labelNeeded': [
                  {
                    'memberLabel': 'Front-end',
                    'count': 1
                  },
                  {
                    'memberLabel': 'Back-end',
                    'count': 2
                  },
                  {
                    'memberLabel': 'Quality-Assurance',
                    'count': 1
                  }
                ],
                'members': null
              }
            ]
          }
        ]
      }
    }

    const wrapper = shallowMount(UserCreateProject, {
      localVue: localVue,
      router,
      computed: {
        projectInitData: jest.fn(() => {
          return recommendationData
        })
      }
    })

    wrapper.setData({ params: params })
    wrapper.vm.goToRecommendation()
  })
})
