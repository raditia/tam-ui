import Vue from 'vue'
import Router from 'vue-router'

// LOGIN
import LoginPage from '../components/user/LoginPage'

// USER
import UserCreateProject from '../components/form/UserCreateProject.vue'
import UserEditProject from '../components/form/UserEditProject'
import UserRecommendationResult from '../components/recommendation/UserRecommendationResult.vue'
import UserProjectList from '../components/projects/UserProjectList.vue'
import UserProjectDetail from '../components/projects/UserProjectDetail.vue'
import UserDashboard from '../components/user/UserDashboard.vue'
import UserAllMembers from '../components/user/UserAllMembers'

// MEMBER
import MemberReviewList from '../components/review/MemberReviewList.vue'
import MemberDashboard from '../components/member/MemberDashboard'
import MemberProfile from '../components/member/MemberProfile'

Vue.use(Router)

const routes = [
  // {
  //   path: '/',
  //   name: 'home',
  //   component: Home,
  //   meta: { requiresAuth: true, userAuth: true, memberAuth: true }
  // },
  {
    path: '/login',
    name: 'login',
    component: LoginPage,
    meta: { requiresAuth: false, userAuth: false, memberAuth: false }
  },
  {
    path: '/dashboard',
    name: 'user-dashboard',
    component: UserDashboard,
    meta: { requiresAuth: true, userAuth: true, memberAuth: false }
  },
  {
    path: '/create',
    name: 'create-project-form',
    component: UserCreateProject,
    meta: { requiresAuth: true, userAuth: true, memberAuth: false }
  },
  {
    path: '/recommendation',
    name: 'recommendation-result',
    component: UserRecommendationResult,
    meta: { requiresAuth: true, userAuth: true, memberAuth: false }
  },
  {
    path: '/project-list',
    name: 'project-list',
    component: UserProjectList,
    meta: { requiresAuth: true, userAuth: true, memberAuth: false }
  },
  {
    path: '/members',
    name: 'member-list',
    component: UserAllMembers,
    meta: { requiresAuth: true, userAuth: true, memberAuth: false }
  },
  {
    path: '/review',
    name: 'review',
    component: MemberReviewList,
    meta: { requiresAuth: true, userAuth: false, memberAuth: true }
  },
  {
    path: '/member',
    name: 'member',
    component: MemberDashboard,
    meta: { requiresAuth: true, userAuth: false, memberAuth: true }
  },
  {
    path: '/profile',
    name: 'profile',
    component: MemberProfile,
    meta: { requiresAuth: true, userAuth: false, memberAuth: true }
  },
  {
    path: '/projects',
    name: 'projects',
    component: UserProjectDetail,
    meta: { requiresAuth: true, userAuth: true, memberAuth: true }
  },
  {
    path: '/edit',
    name: 'edit-project',
    component: UserEditProject,
    meta: { requiresAuth: true, userAuth: true, memberAuth: false }
  }
]

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.meta.requiresAuth) {
    const authUser = JSON.parse(window.localStorage.getItem('user'))
    if (!authUser) {
      next('/login')
    } else if (to.meta.userAuth) {
      const authUser = JSON.parse(window.localStorage.getItem('user'))
      if (authUser.value.member.role.name === 'Manager') {
        next()
      } else if (authUser.value.member.role.name === 'Member') {
        next('/denied')
      }
    } else if (to.meta.memberAuth) {
      const authUser = JSON.parse(window.localStorage.getItem('user'))
      if (authUser.value.member.role.name === 'Member') {
        next()
      } else if (authUser.value.member.role.name === 'Manager') {
        next('/denied')
      }
    }
  } else {
    next()
  }
})

export default router
