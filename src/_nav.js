export default {
  items: [
    {
      type: 'USER',
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'fas fa-home'
    },
    {
      type: 'USER',
      name: 'Create Project',
      url: '/create',
      icon: 'fas fa-plus'
    },
    {
      type: 'USER',
      name: 'Project List',
      url: '/project-list',
      icon: 'fas fa-copy',
      children: [
        {
          name: 'Add New Project',
          url: '/projects/create',
          icon: ''
        },
        {
          name: 'Project List',
          url: '/projects/project-list',
          icon: ''
        }
      ]
    },
    {
      type: 'USER',
      name: 'Member List',
      url: '/members',
      icon: 'fas fa-user-friends'
    },
    {
      type: 'MEMBER',
      name: 'Dashboard',
      url: '/member',
      icon: 'fas fa-home'
    },
    {
      type: 'MEMBER',
      name: 'Profile',
      url: '/profile',
      icon: 'fas fa-user'
    }
  ]
}
