import getters from './getters'
import mutations from './mutations'
import actions from './actions'

const state = {
  projectList: [],
  project: {},
  projectDomain: [],
  projectInit: {}
}

export default {
  state,
  getters,
  mutations,
  actions
}
