const setProjectList = (state, response) => {
  state.projectList = response
}

const setProjectDetail = (state, response) => {
  state.project = response
}

const setProjectDomain = (state, response) => {
  state.projectDomain = response
}

const setProjectInit = (state, response) => {
  state.projectInit = response
}

export default {
  setProjectList,
  setProjectDetail,
  setProjectDomain,
  setProjectInit
}
