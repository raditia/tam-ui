import api from '../../../api'

const getProjectList = ({ commit }, { requestId, success, fail }) => {
  api.getProjectList(requestId, (response) => {
    commit('setProjectList', response.data)
    if (success) success(response)
  }, fail)
}

const getProjectDetail = ({ commit }, { requestId, project, success, fail }) => {
  api.getProjectDetail(requestId, project, (response) => {
    commit('setProjectDetail', response.data)
    if (success) success(response)
  }, fail)
}

const getProjectDomain = ({ commit }, { requestId, success, fail }) => {
  api.getProjectDomain(requestId, (response) => {
    commit('setProjectDomain', response.data)
    if (success) success(response)
  }, fail)
}

const postProjectInit = ({ commit }, { requestId, data, success, fail }) => {
  api.postProjectInit(requestId, (response) => {
    commit('setProjectInit', response.data)
    if (success) success(response)
  }, data, fail)
}

const postCreateProject = ({ commit }, { requestId, data, success, fail }) => {
  api.postCreateProject(requestId, (response) => {
    if (success) success(response)
  }, data, fail)
}

const updateProjectDone = ({ commit }, { requestId, project, success, fail }) => {
  api.updateProjectDone(requestId, project, (response) => {
    if (success) success(response)
  }, fail)
}

const updateProjectRevokeMember = ({ commit }, { requestId, project, member, success, fail }) => {
  api.updateProjectRevokeMember(requestId, project, member, (response) => {
    if (success) success(response)
  }, fail)
}

const updateProjectReactivateMember = ({ commit }, { requestId, projectId, memberId, success, fail }) => {
  api.updateProjectReactivateMember(requestId, projectId, memberId, (response) => {
    if (success) success(response)
  }, fail)
}

const updateProjectAddMember = ({ commit }, { requestId, project, data, success, fail }) => {
  api.updateProjectAddMember(requestId, project, (response) => {
    if (success) success(response)
  }, data, fail)
}

const updateProjectEndDate = ({ commit }, { requestId, project, data, success, fail }) => {
  api.updateProjectEndDate(requestId, project, (response) => {
    if (success) success(response)
  }, data, fail)
}

const searchProject = ({ commit }, { requestId, search, success, fail }) => {
  api.searchProject(requestId, search, (response) => {
    commit('setProjectList', response.data)
    if (success) success(response)
  }, fail)
}

const sortProjectList = ({ commit }, { requestId, sortQuery, success, fail }) => {
  api.sortProjectList(requestId, sortQuery, (response) => {
    commit('setProjectList', response.data)
    if (success) success(response)
  }, fail)
}

export default {
  getProjectList,
  getProjectDetail,
  getProjectDomain,
  postProjectInit,
  postCreateProject,
  updateProjectDone,
  updateProjectRevokeMember,
  updateProjectReactivateMember,
  updateProjectAddMember,
  updateProjectEndDate,
  searchProject,
  sortProjectList
}
