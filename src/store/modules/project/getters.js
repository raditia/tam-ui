const projectList = state => state.projectList
const project = state => state.project
const projectDomain = state => state.projectDomain
const projectInit = state => state.projectInit

export default {
  projectList,
  project,
  projectDomain,
  projectInit
}
