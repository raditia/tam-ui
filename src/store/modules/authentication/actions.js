import api from '../../../api'

const postLoginRequest = ({ commit }, { requestId, data, success, fail }) => {
  api.postLoginRequest(requestId, (response) => {
    commit('setLoginData', response.data)
    if (success) success(response)
  }, data, fail)
}

const getLogoutRequest = ({ commit }, { requestId, success, fail }) => {
  api.getLogoutRequest(requestId, (response) => {
    if (success) success(response)
  }, fail)
}

export default {
  postLoginRequest,
  getLogoutRequest
}
