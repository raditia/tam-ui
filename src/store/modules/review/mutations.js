const setReview = (state, response) => {
  state.review = response
}

const setProjectReview = (state, response) => {
  state.projectReview = response
}

export default {
  setReview,
  setProjectReview
}
