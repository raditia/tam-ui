const review = state => state.review
const projectReview = state => state.projectReview

export default {
  review,
  projectReview
}
