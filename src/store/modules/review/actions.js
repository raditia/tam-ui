import api from '../../../api'

const postMemberReview = ({ commit }, { requestId, data, success, fail }) => {
  api.postMemberReview(requestId, (response) => {
    if (success) success(response)
  }, data, fail)
}

const getReviewsByProjectIdAndReviewerId = ({ commit }, { requestId, projectId, reviewerId, success, fail }) => {
  api.getReviewsByProjectIdAndReviewerId(requestId, projectId, reviewerId, (response) => {
    commit('setProjectReview', response.data)
    if (success) success(response)
  }, fail)
}

const getTargetReviewByProject = ({ commit }, { requestId, projectId, targetId, success, fail }) => {
  api.getTargetReviewByProject(requestId, projectId, targetId, (response) => {
    commit('setReview', response.data)
    if (success) success(response)
  }, fail)
}

export default {
  postMemberReview,
  getReviewsByProjectIdAndReviewerId,
  getTargetReviewByProject
}
