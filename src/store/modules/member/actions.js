import api from '../../../api'

const getMemberList = ({ commit }, { request, success, fail }) => {
  api.getMemberList(request, (response) => {
    commit('setMemberList', response.data)
    if (success) success(response)
  }, fail)
}

const getMemberListAvailable = ({ commit }, { requestId, success, fail }) => {
  api.getMemberListAvailable(requestId, (response) => {
    commit('setMemberListAvailable', response.data)
    if (success) success(response)
  }, fail)
}

const getAMember = ({ commit }, { requestId, memberId, success, fail }) => {
  api.getAMember(requestId, memberId, (response) => {
    commit('setMember', response.data)
    if (success) success(response)
  }, fail)
}

const getMemberDetail = ({ commit }, { requestId, memberId, success, fail }) => {
  api.getMemberDetail(requestId, memberId, (response) => {
    commit('setMemberDetail', response.data)
    if (success) success(response)
  }, fail)
}

const getMemberLabel = ({ commit }, { requestId, success, fail }) => {
  api.getMemberLabel(requestId, (response) => {
    commit('setMemberLabel', response.data)
    if (success) success(response)
  }, fail)
}

const searchMember = ({ commit }, { requestId, search, success, fail }) => {
  api.searchMember(requestId, search, (response) => {
    commit('setMemberList', response.data)
    if (success) success(response)
  }, fail)
}

const searchMemberAvailable = ({ commit }, { requestId, search, success, fail }) => {
  api.searchMemberAvailable(requestId, search, (response) => {
    commit('setMemberListAvailable', response.data)
    if (success) success(response)
  }, fail)
}

const updateMemberPassword = ({ commit }, { requestId, memberId, data, success, fail }) => {
  api.updateMemberPassword(requestId, memberId, (response) => {
    if (success) success(response)
  }, data, fail)
}

const postNewMember = ({ commit }, { requestId, success, data, fail }) => {
  api.postNewMember(requestId, (response) => {
    if (success) success(response)
  }, data, fail)
}

const updateMember = ({ commit }, { requestId, memberId, success, data, fail }) => {
  api.updateMember(requestId, memberId, (response) => {
    if (success) success(response)
  }, data, fail)
}

export default {
  getMemberList,
  getMemberListAvailable,
  getAMember,
  getMemberDetail,
  getMemberLabel,
  searchMember,
  searchMemberAvailable,
  updateMemberPassword,
  postNewMember,
  updateMember
}
