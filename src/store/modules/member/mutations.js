const setMemberList = (state, response) => {
  state.memberList = response
}

const setMemberListAvailable = (state, response) => {
  state.memberListAvailable = response
}

const setMember = (state, response) => {
  state.member = response
}

const setMemberDetail = (state, response) => {
  state.memberDetail = response
}

const setMemberLabel = (state, response) => {
  state.memberLabel = response
}

export default {
  setMemberList,
  setMemberListAvailable,
  setMember,
  setMemberDetail,
  setMemberLabel
}
