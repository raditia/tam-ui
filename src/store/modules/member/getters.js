const memberList = state => state.memberList
const memberListAvailable = state => state.memberListAvailable
const memberDetail = state => state.memberDetail
const memberLabel = state => state.memberLabel
const member = state => state.member

export default {
  memberList,
  memberListAvailable,
  memberDetail,
  memberLabel,
  member
}
