import Vue from 'vue'
import Vuex from 'vuex'
import memberModule from './modules/member/index'
import projectModule from './modules/project/index'
import reviewModule from './modules/review/index'
import authenticationModule from './modules/authentication/index'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    member: memberModule,
    project: projectModule,
    review: reviewModule,
    authentication: authenticationModule
  }
})

export default store
