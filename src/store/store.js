// import Vue from 'vue'
// import Vuex from 'vuex'
// // import api from '@/api'
// import mock from '@/api/mock'
//
// Vue.use(Vuex)
//
// export default new Vuex.Store({
//   state: {
//     projectList: {},
//     projects: {},
//     recommendations: {},
//     members: {},
//     params: {
//       projectName: '',
//       projectType: [],
//       startDate: null,
//       endDate: null,
//       projectDescription: '',
//       labelNeeded: [
//         {
//           label: '',
//           number: ''
//         }
//       ]
//     }
//   },
//   getters: {
//     projectList (state) {
//       return state.projectList
//     },
//     params (state) {
//       return state.params
//     },
//     recommendations (state) {
//       return state.recommendations
//     },
//     members (state) {
//       return state.members
//     },
//     projects (state) {
//       return state.projects
//     }
//   },
//   mutations: {
//     setProjectList (state, value) {
//       state.projectList = value
//     },
//     setProjectDetail (state, value) {
//       state.params = value
//     },
//     setRecommendations (state, value) {
//       state.recommendations = value
//     },
//     setMembers (state, value) {
//       state.members = value
//     },
//     setProjects (state, value) {
//       state.projects = value
//     },
//     setStartDate (state, value) {
//       state.startDate = value
//     },
//     setEndDate (state, value) {
//       state.endDate = value
//     }
//   },
//   actions: {
//     // getProjectList ({ commit }, { data, success, fail } = {}) {
//     //   api.getProjectList((response) => {
//     //     commit('setProjectList', response.data)
//     //     if (success) success(response)
//     //   }, data, fail)
//     // },
//     // getProjectDetail ({ commit }, { data, success, fail } = {}) {
//     //   api.getProjectDetail(response => {
//     //     commit('setProjectDetail', response.data)
//     //     success && success(response)
//     //   }, data, fail)
//     // },
//     // getMemberList ({ commit }, { data, success, fail } = {}) {
//     //   api.getMemberList((response) => {
//     //     commit('setMemberList', response.data)
//     //     if (success) success(response)
//     //   }, data, fail)
//     // },
//     // getMemberDetail ({ commit }, { data, success, fail } = {}) {
//     //   api.getMemberDetail((response) => {
//     //     commit('setMemberDetail', response.data)
//     //     success && success(response)
//     //   }, data, fail)
//     // },
//     getRecommendations ({ commit }) {
//       return mock
//         .fetchRecommendations()
//         .then(recommendations => commit('setRecommendations', recommendations))
//     },
//     getMembers ({ commit }) {
//       return mock
//         .fetchMembers()
//         .then(members => commit('setMembers', members))
//     },
//     getProjects ({ commit }) {
//       return mock
//         .fetchProjects()
//         .then(projects => commit('setProjects', projects))
//     }
//   }
// })
