import axios from 'axios'
import config from './config'
import token from '../js/tokenId'

export default {
  postLoginRequest (request, callback, data, fail) {
    let requestId = encodeURI(request)
    axios.post(config.api.tam.login + '?requestId=' + requestId, data)
      .then(callback)
      .catch(fail)
  },
  getLogoutRequest (request, callback, fail) {
    let requestId = encodeURI(request)
    axios.get(config.api.tam.logout + '?requestId=' + requestId, {
      headers: {
        'token': token.getToken()
      }
    })
      .then(callback)
      .catch(fail)
  },
  getProjectList (request, callback, fail) {
    let requestId = encodeURI(request)

    axios.get(config.api.tam.projects + '?requestId=' + requestId + '&sort=startDate,desc', {
      headers: {
        'token': token.getToken()
      }
    })
      .then(callback)
      .catch(fail)
  },
  sortProjectList (request, sort, callback, fail) {
    let requestId = encodeURI(request)
    let sortQuery = encodeURI(sort)
    axios.get(config.api.tam.projects + '?requestId=' + requestId + '&sort=' + sortQuery, {
      headers: {
        'token': token.getToken()
      }
    })
      .then(callback)
      .catch(fail)
  },
  getProjectDetail (request, project, callback, fail) {
    let projectId = encodeURI(project)
    let requestId = encodeURI(request)
    axios.get(config.api.tam.projects + '/' + projectId + '?requestId=' + requestId, {
      headers: {
        'token': token.getToken()
      }
    })
      .then(callback)
      .catch(fail)
  },
  getProjectDomain (request, callback, fail) {
    let requestId = encodeURI(request)
    axios.get(config.api.tam.projectDomain + '?requestId=' + requestId, {
      headers: {
        'token': token.getToken()
      }
    })
      .then(callback)
      .catch(fail)
  },
  postProjectInit (request, callback, data, fail) {
    let requestId = encodeURI(request)
    axios.post(config.api.tam.projects + '/recommendation' + '?requestId=' + requestId, data, {
      headers: {
        'token': token.getToken()
      }
    })
      .then(callback)
      .catch(fail)
  },
  postCreateProject (request, callback, data, fail) {
    let requestId = encodeURI(request)
    axios.post(config.api.tam.projects + '?requestId=' + requestId, data, {
      headers: {
        'token': token.getToken()
      }
    })
      .then(callback)
      .catch(fail)
  },
  updateProjectDone (request, project, callback, fail) {
    let requestId = encodeURI(request)
    let projectId = encodeURI(project)
    axios.get(config.api.tam.projects + '/' + projectId + '/done' + '?requestId=' + requestId, {
      headers: {
        'token': token.getToken()
      }
    })
      .then(callback)
      .catch(fail)
  },
  updateProjectRevokeMember (request, project, member, callback, fail) {
    let requestId = encodeURI(request)
    let projecId = encodeURI(project)
    let memberId = encodeURI(member)

    axios.get(config.api.tam.projects + '/' + projecId + '/revoke/' + memberId + '?requestId=' + requestId, {
      headers: {
        'token': token.getToken()
      }
    })
      .then(callback)
      .catch(fail)
  },
  updateProjectReactivateMember (request, project, member, callback, fail) {
    let requestId = encodeURI(request)
    let projecId = encodeURI(project)
    let memberId = encodeURI(member)

    axios.get(config.api.tam.projects + '/' + projecId + '/reactivate/' + memberId + '?requestId=' + requestId, {
      headers: {
        'token': token.getToken()
      }
    })
      .then(callback)
      .catch(fail)
  },
  updateProjectAddMember (request, project, callback, data, fail) {
    let requestId = encodeURI(request)
    let projectId = encodeURI(project)

    axios.post(config.api.tam.projects + '/' + projectId + '/add-member' + '?requestId=' + requestId, data, {
      headers: {
        'token': token.getToken()
      }
    })
      .then(callback)
      .catch(fail)
  },
  updateProjectEndDate (request, project, callback, data, fail) {
    let requestId = encodeURI(request)
    let projectId = encodeURI(project)

    axios.put(config.api.tam.projects + '/' + projectId + '/change-end-date' + '?requestId=' + requestId, data, {
      headers: {
        'token': token.getToken()
      }
    })
      .then(callback)
      .catch(fail)
  },
  searchProject (request, search, callback, fail) {
    let requestId = encodeURI(request)
    let searchQuery = encodeURI(search)
    axios.get(config.api.tam.projects + '?requestId=' + requestId + '&key=' + searchQuery, {
      headers: {
        'token': token.getToken()
      }
    })
      .then(callback)
      .catch(fail)
  },
  searchMember (request, search, callback, fail) {
    let requestId = encodeURI(request)
    let searchQuery = encodeURI(search)
    axios.get(config.api.tam.members + '?requestId=' + requestId + '&key=' + searchQuery, {
      headers: {
        'token': token.getToken()
      }
    })
      .then(callback)
      .catch(fail)
  },
  searchMemberAvailable (request, search, callback, fail) {
    let requestId = encodeURI(request)
    let searchQuery = encodeURI(search)
    axios.get(config.api.tam.members + '/available' + '?requestId=' + requestId + '&key=' + searchQuery, {
      headers: {
        'token': token.getToken()
      }
    })
      .then(callback)
      .catch(fail)
  },
  getMemberList (request, callback, fail) {
    let requestId = encodeURI(request)
    axios.get(config.api.tam.members + '?requestId=' + requestId + '&sort=name', {
      headers: {
        'token': token.getToken()
      }
    })
      .then(callback)
      .catch(fail)
  },
  getMemberListAvailable (request, callback, fail) {
    let requestId = encodeURI(request)
    axios.get(config.api.tam.members + '/available' + '?requestId=' + requestId, {
      headers: {
        'token': token.getToken()
      }
    })
      .then(callback)
      .catch(fail)
  },
  getAMember (request, member, callback, fail) {
    let requestId = encodeURI(request)
    let memberId = encodeURI(member)

    axios.get(config.api.tam.members + '/' + memberId + '?requestId=' + requestId, {
      headers: {
        'token': token.getToken()
      }
    })
      .then(callback)
      .catch(fail)
  },
  getMemberDetail (request, member, callback, fail) {
    let requestId = encodeURI(request)
    let memberId = encodeURI(member)
    axios.get(config.api.tam.members + '/' + memberId + '/project-history' + '?requestId=' + requestId, {
      headers: {
        'token': token.getToken()
      }
    })
      .then(callback)
      .catch(fail)
  },
  getMemberLabel (request, callback, fail) {
    let requestId = encodeURI(request)
    axios.get(config.api.tam.memberLabel + '?requestId=' + requestId, {
      headers: {
        'token': token.getToken()
      }
    })
      .then(callback)
      .catch(fail)
  },
  updateMemberPassword (request, member, callback, data, fail) {
    let requestId = encodeURI(request)
    let memberId = encodeURI(member)
    axios.put(config.api.tam.members + '/' + memberId + '/change-password' + '?requestId=' + requestId, data, {
      headers: {
        'token': token.getToken()
      }
    })
      .then(callback)
      .catch(fail)
  },
  postNewMember (request, callback, data, fail) {
    let requestId = encodeURI(request)
    axios.post(config.api.tam.members + '?requestId=' + requestId, data, {
      headers: {
        'token': token.getToken()
      }
    })
      .then(callback)
      .catch(fail)
  },
  updateMember (request, member, callback, data, fail) {
    let requestId = encodeURI(request)
    let memberId = encodeURI(member)
    axios.put(config.api.tam.members + memberId + '/update' + '?requestId=' + requestId, data, {
      headers: {
        'token': token.getToken()
      }
    })
      .then(callback)
      .catch(fail)
  },
  postMemberReview (request, callback, data, fail) {
    let requestId = encodeURI(request)
    axios.post(config.api.tam.review + '?requestId=' + requestId, data, {
      headers: {
        'token': token.getToken()
      }
    })
      .then(callback)
      .catch(fail)
  },
  getReviewsByProjectIdAndReviewerId (request, project, reviewer, callback, fail) {
    let requestId = encodeURI(request)
    let projectId = encodeURI(project)
    let reviewerId = encodeURI(reviewer)
    axios.get(config.api.tam.review + '/' + projectId + '/reviewer/' + reviewerId + '?requestId=' + requestId, {
      headers: {
        'token': token.getToken()
      }
    })
      .then(callback)
      .catch(fail)
  },
  getTargetReviewByProject (request, project, target, callback, fail) {
    let requestId = encodeURI(request)
    let projectId = encodeURI(project)
    let targetId = encodeURI(target)

    axios.get(config.api.tam.review + '/' + projectId + '/show/' + targetId + '?requestId=' + requestId, {
      headers: {
        'token': token.getToken()
      }
    })
      .then(callback)
      .catch(fail)
  }
}
