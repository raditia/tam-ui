const API_BASE_URL = 'http://localhost:8080'
const API_BASE_PATH = '/api'
const API_TAM = API_BASE_URL + API_BASE_PATH + '/tam'

export default {
  api: {
    tam: {
      members: API_TAM + '/members/',
      memberLabel: API_TAM + '/member-label/',
      logout: API_TAM + '/auth/logout',
      login: API_TAM + '/auth/login',
      projects: API_TAM + '/projects',
      project: API_TAM + '/project/',
      projectDomain: API_TAM + '/domain/',
      review: API_TAM + '/reviews',
      recommendation: API_TAM + '/recommendation/'
    }
  },
  app: {
    pages: {
      users: '/users'
    }
  }
}
