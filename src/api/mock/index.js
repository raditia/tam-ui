import recommendations from './data/recommendations.json'
import projects from './data/projects.json'
import members from './data/members.json'

const fetch = (mockData, time = 0) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(mockData)
    }, time)
  })
}

export default {
  fetchRecommendations () {
    return fetch(recommendations, 1000)
  },
  fetchProjects () {
    return fetch(projects, 1000)
  },
  fetchMembers () {
    return fetch(members, 1000)
  }
}
