import requestId from '../requestId'
import { mapGetters } from 'vuex'
import UserSearchMembers from '../../components/user/UserSearchMembers'
import UserAddNewMember from '../../components/user/UserAddNewMember'
import UserEditMember from '../../components/user/UserEditMember'
export default {
  data () {
    return {
      requestId: null
    }
  },
  components: {
    UserSearchMembers,
    UserEditMember,
    UserAddNewMember
  },
  computed: {
    ...mapGetters({
      memberList: 'member/memberList',
      member: 'member/member'
    })
  },
  created () {
    this.getMemberList()
  },
  methods: {
    getMemberList () {
      this.requestId = requestId.createRequestId()
      this.$store.dispatch('member/getMemberList', {
        request: this.requestId,
        success: () => {
          console.log('success')
        },
        fail: () => {
          console.log('gagal')
        }
      })
    },
    getAMember (memberId) {
      this.requestId = requestId.createRequestId()
      this.$store.dispatch('member/getAMember', {
        requestId: this.requestId,
        memberId: memberId,
        success: () => {
          console.log('berhasil dapat member')
        },
        fail: () => {
          console.log('gagaldapat member')
        }
      })
    }
  }
}
