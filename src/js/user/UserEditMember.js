import requestId from '../requestId'
import { mapGetters } from 'vuex'

export default {
  data () {
    return {
      selectedRole: '',
      memberLabelItems: [],
      params: {
        available: true,
        email: '',
        img: '',
        jobLevel: 0,
        label: 0,
        name: '',
        password: '',
        role: 0
      },
      propData: {
        memberName: '',
        memberEmail: '',
        memberLabel: '',
        memberJobLevel: '',
        memberId: '',
        memberImg: ''
      }
    }
  },
  props: {
    memberName: String,
    memberEmail: String,
    memberJobLevel: String,
    memberLabel: String,
    memberId: String,
    memberImg: String
  },
  created () {
    this.getMemberLabel()
    this.initDataFromProps()
  },
  computed: {
    ...mapGetters({
      memberLabelData: 'member/memberLabel'
    })
  },
  methods: {
    getMemberLabel () {
      this.requestId = requestId.createRequestId()
      this.$store.dispatch('member/getMemberLabel', {
        requestId: this.requestId,
        success: (response) => {
          for (let i = 0; i < response.data.value.length; i++) {
            this.memberLabelItems.push(response.data.value[i].name)
          }
        },
        fail: () => {
          console.log('gagal ambil label')
        }
      })
    },
    initDataFromProps () {
      this.propData.memberName = this.memberName
      this.propData.memberEmail = this.memberEmail
      this.propData.memberLabel = this.memberLabel
      this.propData.memberId = this.memberId
      this.propData.memberImg = this.memberImg
      this.propData.memberJobLevel = this.memberJobLevel
    },
    initParams () {
      this.params.name = this.propData.memberName
      this.params.email = this.propData.memberEmail
      this.params.img = this.propData.memberImg

      if (this.propData.memberJobLevel === 'Associate SDE') {
        this.params.jobLevel = 1
      } else if (this.propData.memberJobLevel === 'SDE') {
        this.params.jobLevel = 2
      } else {
        this.params.jobLevel = 3
      }

      if (this.selectedRole === 'Manager') {
        this.params.role = 1
      } else {
        this.params.role = 2
      }

      for (let i = 0; i < this.memberLabelData.value.length; i++) {
        if (this.memberLabelData.value[i].name === this.propData.memberLabel) {
          this.params.label = this.memberLabelData.value[i].id
        }
      }
    },
    updateMember () {
      this.requestId = requestId.createRequestId()
      this.initParams()
      this.$store.dispatch('member/updateMember', {
        requestId: this.requestId,
        memberId: this.memberId,
        data: Object.assign({}, this.params),
        success: () => {
          this.$swal(
            'Updated!',
            'Your member info has been updated',
            'success'
          )
        },
        fail: () => {
          this.$swal(
            'Failed',
            'Something is wrong when updating member',
            'error'
          )
        }
      })
    }
  }
}
