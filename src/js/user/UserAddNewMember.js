import requestId from '../requestId'
import fs from 'fs'
import { mapGetters } from 'vuex'

export default {
  data () {
    return {
      requestId: null,
      memberLabelItems: [],
      selectedMemberLabel: '',
      selectedJobLevel: '',
      selectedRole: '',
      imageName: '',
      imageUrl: '',
      imageFile: '',
      params: {
        available: true,
        email: '',
        img: '',
        jobLevel: 0,
        label: 0,
        name: '',
        password: '',
        role: 0
      }
    }
  },
  mounted () {
    this.getMemberLabel()
  },
  computed: {
    ...mapGetters({
      memberLabel: 'member/memberLabel'
    })
  },
  methods: {
    getMemberLabel () {
      this.requestId = requestId.createRequestId()
      this.$store.dispatch('member/getMemberLabel', {
        requestId: this.requestId,
        success: (response) => {
          for (let i = 0; i < response.data.value.length; i++) {
            this.memberLabelItems.push(response.data.value[i].name)
          }
        },
        fail: () => {
          console.log('gagal ambil label')
        }
      })
    },
    pickFile () {
      this.$refs.image.click()
    },
    onFilePicked (e) {
      const files = e.target.files
      if (files[0] !== undefined) {
        this.imageName = files[0].name
        if (this.imageName.lastIndexOf('.') <= 0) {
          return
        }
        const fr = new FileReader()
        fr.readAsDataURL(files[0])
        fr.addEventListener('load', () => {
          this.imageUrl = fr.result
          this.imageFile = files[0] // this is an image file that can be sent to server...
        })
      } else {
        this.imageName = ''
        this.imageFile = ''
        this.imageUrl = ''
      }
    },
    initParams () {
      for (let i = 0; i < this.memberLabel.value.length; i++) {
        if (this.memberLabel.value[i].name === this.selectedMemberLabel) {
          this.params.label = this.memberLabel.value[i].id
        }
      }

      if (this.selectedJobLevel === 'Associate SDE') {
        this.params.jobLevel = 1
      } else if (this.selectedJobLevel === 'SDE') {
        this.params.jobLevel = 2
      } else {
        this.params.jobLevel = 3
      }

      if (this.selectedRole === 'Manager') {
        this.params.role = 1
      } else {
        this.params.role = 2
      }

      this.params.img = this.imageName
    },
    postNewMember () {
      this.requestId = requestId.createRequestId()
      this.initParams()
      this.$store.dispatch('member/postNewMember', {
        requestId: this.requestId,
        data: Object.assign({}, this.params),
        success: () => {
          this.$swal(
            'Done!',
            'You successfully add new member',
            'success'
          )
          // this.uploadImage(this.imageFile)
        },
        fail: () => {
          this.$swal(
            'Failed!',
            'something is wrong when save new member',
            'error'
          )
        }
      })
    },
    uploadImage (image) {
      let path = '<rootDir>/public/img/avatars/'
      let options = { 'Content-Type': 'multipart/form-data' }
      let formData = new FormData()
      formData.append('image1', image)
      fs.writeFileSync(path, formData, options)
    }
  }
}
