import requestId from '../requestId'
import { mapGetters } from 'vuex'
import UserSearchProject from '../../components/projects/UserSearchProject'
export default {
  name: 'UserDashboard',
  components: {
    UserSearchProject
  },
  data () {
    return {
      requestId: null,
      onGoingProjects: 0,
      plannedProjects: 0,
      endedProjects: 0
    }
  },
  mounted () {
    this.goToDashboard()
  },
  created () {
    this.getProjectList()
  },
  computed: {
    ...mapGetters({
      projectList: 'projectList'
    })
  },
  methods: {
    goToDashboard () {
      this.$swal({
        toast: true,
        title: '<strong>Hi, Welcome back!</strong>',
        position: 'bottom-end',
        type: 'success',
        showConfirmButton: false,
        timer: 3000
      })
    },
    getProjectList () {
      this.requestId = requestId.createRequestId()
      this.$store.dispatch('getProjectList', {
        requestId: this.requestId,
        success: () => {
          this.calculateOnGoingProjects()
          this.calculatePlannedProjects()
          this.calculateEndedProjects()
        },
        fail: () => {
          console.log('gagal fetch data')
        }
      })
    },
    calculateOnGoingProjects () {
      let date = new Date().toISOString()
      for (let i = 0; i < this.projectList.value.content.length; i++) {
        if (this.projectList.value.content[i].status === true && this.projectList.value.content[i].startDate <= date) {
          this.onGoingProjects += 1
        }
      }
    },
    calculatePlannedProjects () {
      let date = new Date().toISOString()
      for (let i = 0; i < this.projectList.value.content.length; i++) {
        if (this.projectList.value.content[i].startDate > date && this.projectList.value.content[i].status === true) {
          this.plannedProjects += 1
        }
      }
    },
    calculateEndedProjects () {
      for (let i = 0; i < this.projectList.value.content.length; i++) {
        if (this.projectList.value.content[i].status === false) {
          this.endedProjects += 1
        }
      }
    }
  }
}
