import requestId from '../requestId'
import { mapGetters } from 'vuex'

export default {
  name: 'LoginPage',
  data () {
    return {
      requestId: null,
      params: {
        email: '',
        password: ''
      }
    }
  },
  computed: {
    ...mapGetters({
      loginData: 'loginData'
    })
  },
  methods: {
    postLoginRequest () {
      this.requestId = requestId.createRequestId()
      this.$store.dispatch('postLoginRequest', {
        requestId: this.requestId,
        data: Object.assign({}, this.params),
        success: () => {
          this.setLoginData()
        },
        fail: () => {
          this.$swal(
            'Login Failed',
            'wrong username or password',
            'error'
          )
        }
      })
    },
    setLoginData () {
      if (this.loginData !== null) {
        window.localStorage.setItem('user', JSON.stringify(this.loginData))
        if (this.loginData.value.member.role.name === 'Member') {
          window.location.assign('/member')
        } else if (this.loginData.value.member.role.name === 'Manager') {
          window.location.assign('/dashboard')
        }
      }
    }
  }
}
