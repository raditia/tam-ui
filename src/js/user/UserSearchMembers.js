import { mapGetters } from 'vuex'
import requestId from '../requestId'

export default {
  data () {
    return {
      items: [],
      search: null,
      select: null,
      loading: false,
      dialog: false,
      requestId: null
    }
  },
  created () {
    this.checkRouteSource()
  },
  computed: {
    ...mapGetters({
      memberList: 'member/memberList',
      memberListAvailable: 'member/memberListAvailable'
    }),
    memberNameList () {
      let list = []
      if (this.$route.path === '/members') {
        for (let index = 0; index < this.memberList.value.length; index++) {
          list.push(this.memberList.value[index].name)
        }
      } else if (this.$route.path === '/recommendation') {
        for (let index = 0; index < this.memberListAvailable.value.length; index++) {
          list.push(this.memberListAvailable.value[index].name)
        }
      }
      return list
    }
  },
  watch: {
    search (val) {
      val && val !== this.select && this.querySelections(val)
    }
  },
  methods: {
    checkRouteSource () {
      if (this.$route.path === '/members') {
        this.getMemberList()
      } else if (this.$route.path === '/recommendation') {
        this.getMemberListAvailable()
      }
    },
    querySelections (v) {
      this.loading = true
      // Simulated ajax query
      setTimeout(() => {
        this.items = this.memberNameList.filter(e => {
          return (e || '').toLowerCase().indexOf((v || '').toLowerCase()) > -1
        })
        this.loading = false
      }, 500)
    },
    searchMember (search) {
      this.requestId = requestId.createRequestId()
      if (this.$route.path === '/members') {
        this.$store.dispatch('member/searchMember', {
          requestId: this.requestId,
          search: search,
          success: () => {
            console.log('berhasil dapat')
          },
          fail: () => {
            console.log('gagal')
          }
        })
      } else if (this.$route.path === '/recommendation') {
        this.$store.dispatch('member/searchMemberAvailable', {
          requestId: this.requestId,
          search: search,
          success: () => {
            console.log('berhasil dapat available')
          },
          fail: () => {
            console.log('gagal')
          }
        })
      }
    },
    getMemberList () {
      this.requestId = requestId.createRequestId()
      this.$store.dispatch('member/getMemberList', {
        request: this.requestId,
        success: () => {
          console.log('berhasil dapat memberlist')
        },
        fail: () => {
          console.log('gagal dapat memberlist')
        }
      })
    },
    getMemberListAvailable () {
      this.requestId = requestId.createRequestId()
      this.$store.dispatch('member/getMemberListAvailable', {
        requestId: this.requestId,
        success: () => {
          console.log('member list available')
        },
        fail: () => {
          console.log('gagal memberlist available')
        }
      })
    }
  }
}
