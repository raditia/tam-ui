import requestId from '../requestId'
import { mapGetters } from 'vuex'

export default {
  name: 'create-project-form',
  data () {
    return {
      requestId: null,
      params: {
        projectName: '',
        description: '',
        startDate: null,
        endDate: null,
        durationDetermined: false,
        labelNeeded: [
          {
            id: null,
            memberLabel: null,
            count: null
          }
        ],
        projectDomain: []
      },
      role: [
        {
          name: '',
          amount: null
        }
      ],
      domainName: [],
      projectNameRules: [
        v => !!v || 'Project name is required',
        v => (v && v.length <= 50) || 'That is a long name. Keep it tight!'
      ],
      search: null,
      startDateMenu: false,
      endDateMenu: false,
      labelItems: [],
      index: 1,
      domainItems: []
    }
  },
  watch: {
    'params.durationNotDetermined': function (val) {
      if (val === true) {
        this.params.endDate = null
      }
    }
  },
  computed: {
    ...mapGetters({
      projectDomainList: 'projectDomain',
      memberLabel: 'member/memberLabel'
    })
  },
  created () {
    this.getProjectDomain()
    this.getMemberLabel()
  },
  methods: {
    goToRecommendation () {
      this.initProjectDomain()
      this.initMemberLabel()
      this.$swal({
        title: '<strong>Wait a moment please...</strong>',
        text: 'We are curating your best men and women',
        timer: 3000,
        onOpen: () => {
          this.$swal.showLoading()
          this.postProjectInit()
        }
      }).then(() => {
        this.$router.push('/recommendation')
      })
    },
    getProjectDomain () {
      this.requestId = requestId.createRequestId()
      this.$store.dispatch('getProjectDomain', {
        requestId: this.requestId,
        success: () => {
          this.pushProjectDomainItems()
        },
        fail: () => {
          console.log('error')
        }
      })
    },
    postProjectInit () {
      this.requestId = requestId.createRequestId()
      this.$store.dispatch('postProjectInit', {
        requestId: this.requestId,
        data: Object.assign({}, this.params),
        success: () => {
          console.log('success post project')
        },
        fail: () => {
          console.log('error post project')
        }
      })
    },
    getMemberLabel () {
      this.requestId = requestId.createRequestId()
      this.$store.dispatch('member/getMemberLabel', {
        requestId: this.requestId,
        success: () => {
          this.pushMemberLabelItems()
          console.log('success get member label')
        },
        fail: () => {
          console.log('error')
        }
      })
    },
    pushProjectDomainItems () {
      for (let i = 0; i < this.projectDomainList.value.length; i++) {
        this.domainItems.push(this.projectDomainList.value[i].name)
      }
    },
    pushMemberLabelItems () {
      for (let i = 0; i < this.memberLabel.value.length; i++) {
        this.labelItems.push(this.memberLabel.value[i].name)
      }
    },
    durationError () {
      this.$swal({
        title: '<strong>Tidak Semudah Itu Ferguso</strong>',
        text: 'Tanggal akhir tidak bisa lebih awal dari tanggal mulai',
        type: 'error'
      })
      this.resetAll()
    },
    resetAll () {
      this.params.startDate = null
      this.params.endDate = null
      this.startDateMenu = false
      this.endDateMenu = false
    },
    addRole: function () {
      this.params.labelNeeded.push({
        id: null,
        count: null,
        memberLabel: null
      })
      this.role.push({
        name: '',
        amount: null
      })
      this.index++
    },
    deleteRow (index) {
      this.role.splice(index, 1)
      this.params.labelNeeded.splice(index, 1)
      this.index--
    },
    initMemberLabel () {
      for (let i = 0; i < this.memberLabel.value.length; i++) {
        for (let j = 0; j < this.role.length; j++) {
          if (this.memberLabel.value[i].name === this.role[j].name) {
            this.params.labelNeeded[j].id = this.memberLabel.value[i].id
            this.params.labelNeeded[j].memberLabel = this.memberLabel.value[i].name
            this.params.labelNeeded[j].count = this.role[j].amount
          }
        }
      }
    },
    initProjectDomain () {
      for (let i = 0; i < this.projectDomainList.value.length; i++) {
        for (let j = 0; j < this.domainName.length; j++) {
          if (this.projectDomainList.value[i].name === this.domainName[j]) {
            this.params.projectDomain.push({
              id: this.projectDomainList.value[i].id,
              name: this.projectDomainList.value[i].name
            })
          }
        }
      }
    }
  }
}
