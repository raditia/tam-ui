import requestId from '../requestId'
import { mapGetters } from 'vuex'

export default {
  data () {
    return {
      requestId: null,
      startDateMenu: false,
      endDateMenu: false,
      startDateFormatted: null,
      endDateFormatted: null,
      params: {
        endDate: null
      }
    }
  },
  computed: {
    ...mapGetters({
      projectDetail: 'project'
    }),
    formattedStartDate () {
      let self = this
      self.startDateFormatted = this.projectDetail.value.startDate
      return self.startDateFormatted ? this.$moment(self.startDateFormatted).format('YYYY-MM-DD') : ''
    },
    projectDomainData () {
      let projectDomainItems = []
      for (let i = 0; i < this.projectDetail.value.projectDomain.length; i++) {
        projectDomainItems.push(this.projectDetail.value.projectDomain[i].name)
      }

      return projectDomainItems
    }
  },
  created () {
    this.formattedEndDate()
  },
  methods: {
    formattedEndDate () {
      let self = this
      self.endDateFormatted = this.projectDetail.value.endDate
      self.params.endDate = this.$moment(self.endDateFormatted).format('YYYY-MM-DD')
    },
    updateProjectEndDate () {
      this.requestId = requestId.createRequestId()
      this.$store.dispatch('updateProjectEndDate', {
        requestId: this.requestId,
        project: this.projectDetail.value.projectId,
        data: Object.assign({}, this.params),
        success: () => {
          this.$swal(
            'Updated!',
            'Project duration has been updated',
            'success'
          )
          setTimeout(() => {
            this.$router.push('/project-list')
          }, 1000)
        },
        fail: () => {
          this.$swal(
            'Failed!',
            'Project duration fails to update',
            'error'
          )
        }
      })
    }
  }
}
