import requestId from '../requestId'
import { mapGetters } from 'vuex'

export default {
  data () {
    return {
      requestId: null,
      memberId: null,
      dialog: false,
      params: {
        newPassword: '',
        oldPassword: ''
      }
    }
  },
  computed: {
    ...mapGetters({
      member: 'member/member'
    })
  },
  created () {
    this.authUser()
  },
  methods: {
    authUser () {
      let user = JSON.parse(window.localStorage.getItem('user'))
      this.memberId = user.value.member.id
      this.$nextTick(() => {
        this.getMemberDetail()
      })
    },
    getMemberDetail () {
      this.requestId = requestId.createRequestId()
      this.$store.dispatch('member/getAMember', {
        requestId: this.requestId,
        memberId: this.memberId,
        success: () => {
          console.log(this.member)
        },
        fail: () => {
          console.log('gagal lihat profile')
        }
      })
    },
    updateMemberPassword () {
      this.requestId = requestId.createRequestId()
      this.$store.dispatch('member/updateMemberPassword', {
        requestId: this.requestId,
        memberId: this.memberId,
        data: Object.assign({}, this.params),
        success: () => {
          this.$swal(
            'Updated!',
            'Your password has successfully changed',
            'success'
          )
        },
        fail: () => {
          this.$swal(
            'Failed!',
            'We cannot change your password right now',
            'error'
          )
        }
      })
    }
  }
}
