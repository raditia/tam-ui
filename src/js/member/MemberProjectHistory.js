import requestId from '../requestId'

export default {
  data () {
    return {
      requestId: null
    }
  },
  props: {
    projectId: String,
    projectName: String,
    projectDescription: String,
    projectStartDate: String,
    projectEndDate: String,
    projectStatus: Boolean,
    projectDomains: Array,
    projectLabelNeeded: Array,
    projectMembers: Object
  },
  computed: {
    getCurrentDate () {
      return new Date().toISOString()
    }
  },
  methods: {
    getProjectDetail (projectId) {
      this.requestId = requestId.createRequestId()
      this.$store.dispatch('getProjectDetail', {
        requestId: this.requestId,
        project: projectId,
        success: () => {
          this.$router.push('/review')
        },
        fail: () => {
          console.log('error')
        }
      })
    }
  }
}
