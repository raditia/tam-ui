import requestId from '../requestId'
import MemberProjectCardHistory from '../../components/member/MemberProjectCardHistory'
import { mapGetters } from 'vuex'

export default {
  data () {
    return {
      projectMembers: [],
      reversedProjectMembers: []
    }
  },
  components: {
    MemberProjectCardHistory
  },
  computed: {
    ...mapGetters({
      memberDetailData: 'member/memberDetail',
      project: 'project'
    })
  },
  created () {
    setTimeout(() => {
      this.getMemberDetail()
    }, 500)
  },
  methods: {
    getMemberDetail () {
      let user = JSON.parse(window.localStorage.getItem('user'))
      let userId = user.value.member.id
      this.requestId = requestId.createRequestId()
      this.$store.dispatch('member/getMemberDetail', {
        requestId: this.requestId,
        memberId: userId,
        success: () => {
          this.memberDetailData.value.content.reverse()
          for (let i = 0; i < this.memberDetailData.value.content.length; i++) {
            this.getProjectDetail(this.memberDetailData.value.content[i].id)
          }
          this.projectMembers.sort()
          console.log('berhasil dapat member detail')
        },
        fail: () => {
          console.log('gagal dapat member detail')
        }
      })
    },
    getProjectDetail (projectId) {
      this.requestId = requestId.createRequestId()
      this.$store.dispatch('getProjectDetail', {
        requestId: this.requestId,
        project: projectId,
        success: () => {
          let members = []
          for (let i = 0; i < this.project.value.members.length; i++) {
            members.push(this.project.value.members[i])
          }
          this.projectMembers.push({
            id: this.project.value.projectId,
            memberList: members
          })
        },
        fail: () => {
          console.log('gagal mendapatkan project detail')
        }
      })
    }
  }
}
