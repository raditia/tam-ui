import { mapGetters } from 'vuex'
import requestId from '../requestId'
import UserEditRecommendation from '../../components/recommendation/UserEditRecommendation'
import MemberProjectCard from '../../components/review/MemberProjectCard'

export default {
  name: 'recommendation-result',
  components: {
    UserEditRecommendation,
    MemberProjectCard
  },
  data () {
    return {
      requestId: null,
      dialog: false,
      id: '',
      show: false,
      loading: false,
      loader: null,
      recommendationMembers: [],
      renderComponent: true,
      params: {
        projectName: '',
        description: '',
        startDate: null,
        endDate: null,
        manager: '',
        durationDetermined: false,
        labelNeeded: [],
        projectDomain: [],
        members: []
      }
    }
  },
  computed: {
    ...mapGetters({
      projectInitData: 'projectInit',
      memberLabel: 'member/memberLabel'
    }),
    checkMemberRecommendation () {
      let labelNeededCount = 0
      for (let i = 0; i < this.projectInitData.value.labelNeeded.length; i++) {
        labelNeededCount += this.projectInitData.value.labelNeeded[i].count
      }

      return this.projectInitData.value.members.length === labelNeededCount
    }
  },
  mounted () {
    this.getManagerId()
  },
  created () {
    this.initRecommendationMembers()
  },
  methods: {
    getManagerId () {
      let user = JSON.parse(window.localStorage.getItem('user'))
      this.params.manager = user.value.member.id
    },
    getCardID: function (evt) {
      this.id = evt
      this.show = this.show === false
    },
    postCreateProject () {
      this.requestId = requestId.createRequestId()
      this.initParams()

      this.$store.dispatch('postCreateProject', {
        requestId: this.requestId,
        data: Object.assign({}, this.params),
        success: () => {
          console.log('success create project')
        },
        fail: () => {
          console.log('error')
        }
      })
    },
    initParams () {
      this.params.projectName = this.projectInitData.value.projectName
      this.params.description = this.projectInitData.value.description
      this.params.startDate = this.projectInitData.value.startDate
      this.params.endDate = this.projectInitData.value.endDate
      this.params.durationDetermined = this.projectInitData.value.durationDetermined
      for (let i = 0; i < this.projectInitData.value.labelNeeded.length; i++) {
        this.params.labelNeeded.push(this.projectInitData.value.labelNeeded[i])
      }
      for (let i = 0; i < this.projectInitData.value.projectDomain.length; i++) {
        this.params.projectDomain.push(this.projectInitData.value.projectDomain[i])
      }
      for (let i = 0; i < this.memberLabel.value.length; i++) {
        for (let j = 0; j < this.recommendationMembers.length; j++) {
          if (this.memberLabel.value[i].name === this.recommendationMembers[j].label) {
            this.params.members.push({
              memberId: this.recommendationMembers[j].id,
              labelId: this.memberLabel.value[i].id
            })
          }
        }
      }
    },
    initRecommendationMembers () {
      for (let i = 0; i < this.projectInitData.value.members.length; i++) {
        this.recommendationMembers.push(this.projectInitData.value.members[i])
      }
    },
    updateRecommendationMembers (member, prevMember, prevMemberLabelData) {
      console.log(member)
      console.log(prevMember)
      let index

      for (let i = 0; i < this.recommendationMembers.length; i++) {
        if (this.recommendationMembers[i].id === prevMember) {
          index = i
        }
      }

      this.$set(this.recommendationMembers, index, member)
      this.recommendationMembers[index].label = prevMemberLabelData

      // Remove component from the DOM
      this.renderComponent = false
      setTimeout(() => {
        this.renderComponent = true
      }, 500)
      console.log(this.recommendationMembers)
    },
    acceptTeam () {
      this.$swal({
        title: '<strong>Excellent Choice!</strong>',
        text: 'Wait a moment please...',
        timer: 3000,
        onOpen: () => {
          this.$swal.showLoading()
          this.postCreateProject()
        }
      }).then((response) => {
        if (response) {
          this.$swal({
            title: 'Yeay!',
            text: 'Successfully add project 😉',
            type: 'success',
            onAfterClose: () => {
              this.$router.push('/project-list')
            }
          })
        } else {
          this.$swal({
            type: 'error',
            title: 'Oops...',
            text: 'Something went wrong!'
          })
        }
      })
    }
  }
}
