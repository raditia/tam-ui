import requestId from '../requestId'
import { mapGetters } from 'vuex'

export default {
  data () {
    return {
      requestId: null,
      params: {
        labelId: 0,
        memberId: ''
      },
      otherMembers: []
    }
  },
  props: {
    idData: String,
    labelData: String
  },
  computed: {
    ...mapGetters({
      memberList: 'member/memberListAvailable',
      memberLabel: 'member/memberLabel',
      projectInitData: 'projectInit'
    })
  },
  mounted () {
    this.getMemberList()
  },
  created () {
    this.seeData()
  },
  methods: {
    seeData () {
      console.log('masuk: ' + this.idData)
      console.log('masuk: ' + this.labelData)
    },
    getMemberList: function () {
      this.requestId = requestId.createRequestId()
      this.$store.dispatch('member/getMemberListAvailable', {
        requestId: this.requestId,
        success: () => {
          this.checkMember()
        },
        fail: () => {
          console.log('gagal')
        }
      })
    },
    checkMember () {
      for (let i = 0; i < this.memberList.value.length; i++) {
        if (this.memberList.value[i].id !== this.idData) {
          this.otherMembers.push(this.memberList.value[i])
        }
      }
      console.log(this.otherMembers)
    },
    chooseMemberModal (member, prevMember, prevMemberLabelData) {
      this.$swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, update it!'
      }).then((result) => {
        if (result.value) {
          this.updateMemberRecommendation(member, prevMember, prevMemberLabelData)
          this.$swal(
            'Updated!',
            'Your team has been updated!',
            'success'
          )
        }
      })
    },
    updateMemberRecommendation (member, prevMember, prevMemberLabelData) {
      this.$emit('clicked', member, prevMember, prevMemberLabelData)
    }
  }
}
