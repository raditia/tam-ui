import UserOtherRecommendation from '../../components/recommendation/UserOtherRecommendation'
import UserSearchMember from '../../components/user/UserSearchMembers'
export default {
  data () {
    return {

    }
  },
  components: {
    UserOtherRecommendation,
    UserSearchMember
  },
  props: {
    memberId: String,
    memberLabel: String
  },
  created () {
    this.getData()
  },
  methods: {
    getData () {
      console.log('memberId: ' + this.memberId)
      console.log('memberLabel: ' + this.memberLabel)
    },
    chosenData (member, prevMember, prevMemberLabelData) {
      this.$emit('chosen', member, prevMember, prevMemberLabelData)
    }
  }
}
