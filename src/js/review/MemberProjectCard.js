import { mapGetters } from 'vuex'
export default {
  data () {
    return {
      numberOfIndividuals: 0,
      roleNeeded: [],
      domainNeeded: []
    }
  },
  computed: {
    ...mapGetters({
      memberLabel: 'member/memberLabel',
      projectDomain: 'projectDomain'
    }),
    authUser () {
      let user = JSON.parse(window.localStorage.getItem('user'))

      return user.value.member.role.name
    }
  },
  created () {
    this.fetchData()
  },
  props: {
    projectName: String,
    projectDescription: String,
    startDate: String,
    endDate: String,
    projectDomainCard: Array,
    members: Array,
    memberLabelCard: Array,
    projectStatus: Boolean
  },
  methods: {
    fetchData () {
      setTimeout(() => {
        this.calculateNumberOfIndividuals()
      }, 500)
    },
    calculateNumberOfIndividuals () {
      for (let i = 0; this.memberLabelCard.length; i++) {
        this.numberOfIndividuals += this.memberLabelCard[i].count
      }
    },
    editProject () {
      this.$router.push('/edit')
    }
  }
}
