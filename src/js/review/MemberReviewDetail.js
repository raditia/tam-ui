import requestId from '../requestId'

export default {
  data () {
    return {
      requestId: null,
      emptyIcon: 'far fa-frown-open',
      fullIcon: 'fas fa-smile',
      hover: true,
      length: 5,
      params: {
        communicationScore: 0,
        domainScore: 0,
        leadershipScore: 0,
        problemSolvingScore: 0,
        project: '',
        rateScore: 0,
        responsibilityScore: 0,
        reviewer: '',
        target: '',
        teamWorkScore: 0,
        technicalScore: 0
      }
    }
  },
  props: {
    userRole: String,
    projectId: String,
    memberId: String,
    img: String,
    memberName: String,
    jobLevel: String,
    labels: String,
    memberRecords: Object
  },
  methods: {
    calculateRateScore () {
      let total = 0
      let result = 0
      total = this.params.communicationScore + this.params.domainScore + this.params.leadershipScore + this.params.problemSolvingScore +
        this.params.responsibilityScore + this.params.teamWorkScore + this.params.technicalScore
      result = total / 7

      this.params.rateScore = Math.round(result * 100) / 100
    },
    initParams () {
      let reviewer = JSON.parse(window.localStorage.getItem('user'))
      this.calculateRateScore()

      this.params.project = this.projectId
      this.params.target = this.memberId
      this.params.reviewer = reviewer.value.member.id
    },
    postReview () {
      this.requestId = requestId.createRequestId()
      this.initParams()

      this.$store.dispatch('postMemberReview', {
        requestId: this.requestId,
        data: Object.assign({}, this.params),
        success: () => {
          this.$swal(
            'Done!',
            'Berhasil menambahkan Review',
            'success'
          )
          setTimeout(() => {
            this.getProjectReview()
          }, 1000)
        },
        fail: () => {
          console.log('gagal post review')
        }
      })
    },
    getProjectReview () {
      this.requestId = requestId.createRequestId()
      this.$store.dispatch('getReviewsByProjectIdAndReviewerId', {
        requestId: this.requestId,
        projectId: this.projectId,
        reviewerId: this.params.reviewer,
        success: () => {
          console.log('berhasil update data project review')
        },
        fail: () => {
          console.log('gagal update data project review')
        }
      })
    }
  }
}
