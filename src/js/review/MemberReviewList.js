import { mapGetters } from 'vuex'
import requestId from '../requestId'
import MemberProjectCard from '../../components/review/MemberProjectCard'
import MemberReviewCard from '../../components/review/MemberReviewCard'

export default {
  components: {
    MemberProjectCard,
    MemberReviewCard
  },
  data () {
    return {
      memberId: ''
    }
  },
  created () {
    this.getMemberId()
    this.getProjectReview()
  },
  computed: {
    ...mapGetters({
      projectDetail: 'project',
      projectReviewData: 'projectReview'
    })
  },
  methods: {
    getMemberId () {
      let memberId = JSON.parse(window.localStorage.getItem('user'))
      this.memberId = memberId.value.member.id
      console.log(this.memberId)
    },
    getProjectReview () {
      this.requestId = requestId.createRequestId()
      let user = JSON.parse(window.localStorage.getItem('user'))
      let userId = user.value.member.id

      this.$store.dispatch('getReviewsByProjectIdAndReviewerId', {
        requestId: this.requestId,
        projectId: this.projectDetail.value.projectId,
        reviewerId: userId,
        success: () => {
          console.log('berhasil update data project review')
          console.log(this.projectReviewData)
        },
        fail: () => {
          console.log('gagal update data project review')
        }
      })
    }
  }

}
