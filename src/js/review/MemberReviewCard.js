import MemberReviewDetail from '../../components/review/MemberReviewDetail'

export default {
  components: {
    MemberReviewDetail
  },
  data () {
    return {
      dialog: false,
      userRole: ''
    }
  },
  created () {
    this.getUserRole()
  },
  props: {
    projectId: String,
    projectStatus: Boolean,
    id: String,
    img: String,
    memberName: String,
    jobLevel: String,
    labels: String,
    records: Object,
    statusOnProject: Boolean,
    statusReview: Boolean
  },
  methods: {
    getUserRole () {
      let user = JSON.parse(window.localStorage.getItem('user'))
      this.userRole = user.value.member.role.name
    }
  }
}
