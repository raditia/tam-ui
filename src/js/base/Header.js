import requestId from '../requestId'

export default {
  data () {
    return {
      requestId: null,
      username: '',
      userImage: '',
      clipped: false,
      drawer: true,
      fixed: false,
      miniVariant: false,
      right: true,
      rightDrawer: false,
      title: 'Team Assignment Management',
      items: [
        'Logout'
      ]
    }
  },
  mounted () {
    this.getUserInfo()
  },
  methods: {
    performDrawer () {
      this.drawer = !this.drawer
      this.$root.$emit('drawer')
    },
    performMiniVariant () {
      this.miniVariant = !this.miniVariant
      this.$root.$emit('miniVariant')
    },
    getUserInfo () {
      let user = JSON.parse(window.localStorage.getItem('user'))
      this.username = user.value.member.name
      this.userImage = user.value.member.img
    },
    getLogoutRequest () {
      this.requestId = requestId.createRequestId()
      this.$store.dispatch('getLogoutRequest', {
        requestId: this.requestId,
        success: () => {
          window.localStorage.removeItem('user')
          setTimeout(() => {
            window.location.reload()
          }, 1000)
        }
      })
    }
  }
}
