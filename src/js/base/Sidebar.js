import nav from '../../_nav.js'

export default {
  data () {
    return {
      clipped: false,
      fixed: false,
      drawer: true,
      miniVariant: false,
      right: true,
      rightDrawer: false,
      nav: nav.items
    }
  },
  props: {
  },
  mounted () {
    this.$root.$on('drawer', () => {
      this.drawer = !this.drawer
    })
    this.$root.$on('miniVariant', () => {
      this.miniVariant = !this.miniVariant
    })
  },
  computed: {
    authUser () {
      let user = JSON.parse(window.localStorage.getItem('user'))
      return user.value.member.role.name
    },
    navigationList () {
      let navList = []

      if (this.authUser === 'Manager') {
        for (let i = 0; i < this.nav.length; i++) {
          if (this.nav[i].type === 'USER') {
            navList.push(this.nav[i])
          }
        }
      } else if (this.authUser === 'Member') {
        for (let i = 0; i < this.nav.length; i++) {
          if (this.nav[i].type === 'MEMBER') {
            navList.push(this.nav[i])
          }
        }
      }

      return navList
    }
  },
  methods: {
    goToLink (evt) {
      window.location.assign(evt)
    }
  }
}
