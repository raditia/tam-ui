export default class TokenId {
  static getToken () {
    const tokenData = JSON.parse(window.localStorage.getItem('user'))

    return tokenData.value.token
  }
}
