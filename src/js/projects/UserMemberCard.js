import { mapGetters } from 'vuex'
import requestId from '../requestId'
import MemberReviewDetail from '../../components/review/MemberReviewDetail'
import UserRevokeMember from '../../components/projects/UserRevokeMember'

export default {
  components: {
    MemberReviewDetail,
    UserRevokeMember
  },
  data () {
    return {
      dialog: null,
      requestId: null,
      userRole: '',
      memberRecord: null
    }
  },
  computed: {
    ...mapGetters({
      project: 'project',
      memberReview: 'review'
    }),
    calculateDateRange () {
      let currentDate = new Date().toISOString()
      return this.project.value.endDate >= currentDate
    }
  },
  created () {
    this.getUserRole()
  },
  props: {
    projectId: String,
    id: String,
    img: String,
    memberName: String,
    jobLevel: String,
    labels: String,
    records: Object,
    statusOnProject: Boolean,
    projectStatus: Boolean,
    memberStatus: Boolean
  },
  methods: {
    getUserRole () {
      let user = JSON.parse(window.localStorage.getItem('user'))
      this.userRole = user.value.member.role.name

      if (user.value.member.role.name === 'Manager') {
        this.getTargetReviewByProject()
      }
    },
    showRevokeAlert (projectId, memberId) {
      this.$swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, revoke it!'
      }).then((result) => {
        if (result.value) {
          this.updateProjectRevokeMember(projectId, memberId)
        }
      })
    },
    showReactivateAlert (projectId, memberId) {
      this.$swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Confirm activation!'
      }).then((result) => {
        if (result.value) {
          this.updateProjectReactivateMember(projectId, memberId)
        }
      })
    },
    updateProjectReactivateMember (projectId, memberId) {
      this.requestId = requestId.createRequestId()
      this.$store.dispatch('updateProjectReactivateMember', {
        requestId: this.requestId,
        projectId: projectId,
        memberId: memberId,
        success: () => {
          this.$emit('updated', true)
          this.$swal(
            'Done!',
            'One of the team has been reactivated.',
            'success'
          )
        },
        fail: () => {
          this.$swal(
            'Failed!',
            'Something is wrong when updating team member.',
            'error'
          )
        }
      })
    },
    updateProjectRevokeMember (projectId, memberId) {
      this.requestId = requestId.createRequestId()
      this.$store.dispatch('updateProjectRevokeMember', {
        requestId: this.requestId,
        project: projectId,
        member: memberId,
        success: () => {
          this.$emit('updated', true)
          this.$swal(
            'Done!',
            'One of the team has been revoked.',
            'success'
          )
        },
        fail: () => {
          console.log('gagal revoke member')
        }
      })
    },
    getTargetReviewByProject () {
      this.requestId = requestId.createRequestId()
      this.$store.dispatch('getTargetReviewByProject', {
        requestId: this.requestId,
        projectId: this.projectId,
        targetId: this.id,
        success: () => {
          console.log('berhasil dapat yo')
          this.memberRecord = this.memberReview.value.records
        },
        fail: () => {
          console.log('gagal')
        }
      })
    }
  }
}
