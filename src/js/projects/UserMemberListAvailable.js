import requestId from '../requestId'
import { mapGetters } from 'vuex'

export default {
  data () {
    return {
      requestId: null,
      params: {
        labelId: 0,
        memberId: ''
      }
    }
  },
  props: {
    projectId: String
  },
  computed: {
    ...mapGetters({
      memberList: 'member/memberListAvailable',
      memberLabel: 'member/memberLabel'
    })
  },
  mounted () {
    this.getMemberList()
  },
  methods: {
    getMemberList: function () {
      this.requestId = requestId.createRequestId()
      this.$store.dispatch('member/getMemberListAvailable', {
        requestId: this.requestId,
        success: (response) => {
          console.log('berhasil ambil member list')
          console.log(response.data)
        },
        fail: () => {
          console.log('gagal')
        }
      })
    },
    selectMember (memberId, labelName) {
      for (let i = 0; i < this.memberLabel.value.length; i++) {
        if (labelName === this.memberLabel.value[i].name) {
          this.params.labelId = this.memberLabel.value[i].id
        }
      }
      this.$swal({
        title: 'Are you sure this guy fits in the project?',
        text: "You won't be able to revert this!",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sure!'
      }).then((result) => {
        if (result.value) {
          this.updateProjectAddMember(this.projectId, memberId, this.params.labelId)
        }
      })
    },
    updateProjectAddMember (projectId, memberId, labelId) {
      this.requestId = requestId.createRequestId()
      this.params.memberId = memberId
      this.params.labelId = labelId
      this.$store.dispatch('updateProjectAddMember', {
        requestId: this.requestId,
        project: projectId,
        data: Object.assign({}, this.params),
        success: () => {
          this.$swal(
            'Done!',
            'Enjoy your new member :).',
            'success'
          )
          this.$router.push('/project-list')
        },
        fail: () => {
          console.log('gagal tambah member')
        }
      })
    }
  }
}
