import { mapGetters } from 'vuex'
import UserMemberCard from '../../components/projects/UserMemberCard'
import requestId from '../requestId'

export default {
  data () {
    return {
      dialog: false,
      renderComponent: false
    }
  },
  components: {
    UserMemberCard,
    UserChangeTeamMemberModal: () => import('../../components/projects/UserChangeTeamMemberModal')
  },
  computed: {
    ...mapGetters({
      project: 'project'
    })
  },
  methods: {
    reRenderComponent (value) {
      this.getProjectDetail()
      this.renderComponent = value
      setTimeout(() => {
        this.renderComponent = false
      }, 1000)
    },
    getProjectDetail () {
      this.requestId = requestId.createRequestId()
      this.$store.dispatch('getProjectDetail', {
        requestId: this.requestId,
        project: this.project.value.projectId,
        success: () => {
          console.log('berhasil get')
        },
        fail: () => {
          console.log('error')
        }
      })
    }
  }
}
