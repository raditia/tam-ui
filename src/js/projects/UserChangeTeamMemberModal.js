import UserMemberListAvailable from '../../components/projects/UserMemberListAvailable'
import UserSearchMembers from '../../components/user/UserSearchMembers'

export default {
  components: {
    UserMemberListAvailable,
    UserSearchMembers
  },
  props: {
    projectId: String
  },
  data () {
    return {

    }
  }
}
