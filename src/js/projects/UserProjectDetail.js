import { mapGetters } from 'vuex'
import MemberProjectCard from '../../components/review/MemberProjectCard'
import UserMemberList from '../../components/projects/UserMemberList'

export default {
  components: {
    MemberProjectCard,
    UserMemberList
  },
  computed: {
    ...mapGetters({
      projectDetail: 'project'
    })
  },
  created () {
    this.showResponse()
  },
  data () {
    return {

    }
  },
  methods: {
    showResponse () {
      console.log(this.projectDetail)
    }
  }
}
