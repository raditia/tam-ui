import { mapGetters } from 'vuex'
import requestId from '../requestId'
import CreateProjectForm from '../../components/form/UserCreateProject'

export default {
  components: {
    CreateProjectForm
  },
  data () {
    return {
      items: [],
      search: null,
      select: null,
      loading: false,
      dialog: false,
      requestId: null
    }
  },
  computed: {
    ...mapGetters({
      projects: 'projectList'
    }),
    ProjectNameList () {
      let list = []
      for (let index = 0; index < this.projects.value.content.length; index++) {
        list.push(this.projects.value.content[index].name)
      }
      return list
    }
  },
  mounted () {
    this.searchInit()
  },
  watch: {
    search (val) {
      val && val !== this.select && this.querySelections(val)
    }
  },
  methods: {
    querySelections (v) {
      this.loading = true
      // Simulated ajax query
      setTimeout(() => {
        this.items = this.ProjectNameList.filter(e => {
          return (e || '').toLowerCase().indexOf((v || '').toLowerCase()) > -1
        })
        this.loading = false
      }, 500)
    },
    searchProject (search) {
      this.requestId = requestId.createRequestId()
      this.$store.dispatch('searchProject', {
        requestId: this.requestId,
        search: search,
        success: () => {
          console.log('berhasil filter project')
        },
        fail: () => {
          console.log('gagal')
        }
      })
    },
    searchInit () {
      this.$root.$emit('UserSearchProject')
    }
  }
}
