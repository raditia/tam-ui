import { mapGetters } from 'vuex'
import requestId from '../requestId'
import UserSearchProject from '../../components/projects/UserSearchProject'

export default {
  components: {
    UserSearchProject
  },
  data () {
    return {
      page: 1,
      sortItems: [
        { title: 'Project Name', query: 'name' },
        { title: 'Start Date', query: 'startDate' },
        { title: 'End Date', query: 'endDate' }
      ],
      onGoing: true,
      projectMembers: [],
      panel: false,
      projectListData: [
        {
          id: '',
          name: '',
          description: '',
          startDate: null,
          endDate: null,
          projectDomains: [],
          projectLabelNeededs: [],
          members: []
        }
      ],
      totalNeeded: 0
    }
  },
  computed: {
    ...mapGetters({
      projects: 'projectList',
      project: 'project'
    }),
    sortProjectMembers () {
      let app = this
      return app.projectMembers.sort()
    },
    getCurrentDate () {
      return new Date().toISOString()
    }
  },
  created () {
    setTimeout(() => {
      this.getProjectList()
    }, 1000)
  },
  methods: {
    getProjectList () {
      this.requestId = requestId.createRequestId()
      this.$store.dispatch('getProjectList', {
        requestId: this.requestId,
        success: () => {
          console.log('Project List Berhasil')
          for (let i = 0; i < this.projects.value.content.length; i++) {
            this.getProjectMembers(this.projects.value.content[i].id)
          }
        },
        fail: () => {
          console.log('error')
        }
      })
    },
    getProjectMembers (projectId) {
      this.requestId = requestId.createRequestId()
      this.$store.dispatch('getProjectDetail', {
        requestId: this.requestId,
        project: projectId,
        success: () => {
          let members = []
          for (let i = 0; i < this.project.value.members.length; i++) {
            members.push(this.project.value.members[i])
          }
          this.projectMembers.push({
            id: this.project.value.projectId,
            memberList: members
          })
        },
        fail: () => {
          console.log('error')
        }
      })
    },
    getProjectDetail (projectId) {
      this.requestId = requestId.createRequestId()
      this.$store.dispatch('getProjectDetail', {
        requestId: this.requestId,
        project: projectId,
        success: () => {
          console.log('berhasil')
          this.$router.push('/projects')
        },
        fail: () => {
          console.log('error')
        }
      })
    },
    markProjectAsDoneModal (projectId) {
      this.$swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#2196F3',
        cancelButtonColor: '#FF5252',
        confirmButtonText: 'Mark as Done!'
      }).then((result) => {
        if (result.value) {
          this.updateProjectAsDone(projectId)
        }
      })
    },
    updateProjectAsDone (projectId) {
      this.requestId = requestId.createRequestId()
      this.$store.dispatch('updateProjectDone', {
        requestId: this.requestId,
        project: projectId,
        success: () => {
          this.$swal(
            'Marked as Done!',
            'Your project has been ended.',
            'success'
          )
          this.projectMembers = []
          setTimeout(() => {
            this.getProjectList()
          }, 1000)
        },
        fail: () => {
          this.$swal({
            type: 'error',
            title: 'Oops...',
            text: 'Something went wrong!'
          })
        }
      })
    },
    sortProjectList (query) {
      this.requestId = requestId.createRequestId()
      this.$store.dispatch('sortProjectList', {
        requestId: this.requestId,
        sortQuery: query,
        success: () => {
          console.log('berhasil sort')
        },
        fail: () => {
          console.log('gagal sort')
        }
      })
    }
  }
}
