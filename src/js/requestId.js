export default class RequestId {
  static createRequestId () {
    return Math.round((new Date()).getTime() / 1000)
  }
}
